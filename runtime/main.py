import sys
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QFileDialog 
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QBoxLayout
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QSize, Qt, QEvent
from MainWindow import Ui_MainWindow

from ImageHolder import ImageHolder


class MainWindow(QMainWindow, Ui_MainWindow):
    
    #layout, into which icons shall be added
    imagesLayout = QBoxLayout(QBoxLayout.TopToBottom)
    iconSizes = QSize(256, 256)

    #init
    def __init__(self, *args, **kwargs):
       
        super().__init__(*args, **kwargs)
        self.setupUi(self)
        self.InitializeUi()

  
    #Addinational initialization of ui, connecting all events...
    def InitializeUi(self):
        
        # preparing scroll area - into the scroll area a layout must be inserted. Into this layout
        # loaded images are added later
        holder = ImageHolder(self.scrollArea)
        holder.setLayout(self.imagesLayout)
        holder.setAcceptDrops(True)
        self.scrollArea.setWidget(holder)
        self.scrollArea.setMinimumSize(self.iconSizes + QSize(0, 10))
        self.scrollArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        

        #connect buttons here:
        self.OpenButton.released.connect(self.LoadSVGIcon)


    #Loads svg via dialog
    def LoadSVGIcon(self):

        retval = QFileDialog.getOpenFileNames(self, "Dialog", "/home/", "Image (*.svg)")
        for item in retval[0]:
            self.AppendImage(item)


    #Creates and inserts new image specified by path into scrollable widget
    def AppendImage(self, iconPath):

        print(iconPath)
        icon = QIcon(iconPath)
        if icon is None:
            print("Unable to create icon from provided one, make sure its really icon, otherwise unsupported")
            return
        button = QPushButton(self.scrollArea)
        button.setAutoExclusive(True)
        button.setCheckable(True)
        button.setIcon(icon)
        button.setMinimumSize(self.iconSizes)
        button.setIconSize(button.minimumSize())
        button.toggled.connect(self.EnableExport)
        self.imagesLayout.addWidget(button)
    

    #Enables export to be pressed
    def EnableExport(self, checked):

        if checked:
            self.ExportButton.setEnabled(True)


    #User can also just import via drag&drop
    def dropEvent(self, e):

        e.acceptProposedAction()
        print(e)

    def dragEnterEvent(self, e):

        paths = []
        if e.mimeData().hasUrls():
            for path in e.mimeData().urls():
                print(path)
                if (path.toLocalFile()[-4:] == ".svg"):
                    paths.append(path.toLocalFile())
            
            for path in paths:
                self.AppendImage(path)

        else:
            e.ignore()
    

app = QApplication(sys.argv)
window = MainWindow()
window.show()
app.exec_()